from django.shortcuts import render, redirect
from django.db.models import Q
from brand.models import Brand
from authentication.models import User
from market.models import Market
from product.models import Product
from category.models import Category
from django.views.generic import DetailView, ListView, UpdateView, DeleteView
from cheque.models import Cheque
from main.forms import ChequeForm
from rest_framework.pagination import PageNumberPagination
from rest_framework import filters
import datetime

from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError,NotFound, AuthenticationFailed
from rest_framework.permissions import IsAuthenticated

from rest_framework_simplejwt.tokens import RefreshToken

from brand.serializers import BrandSerializer
from authentication.serializers import UserSerializer
from category.serializers import CategorySerializer
from market.serializers import MarketSerializer
from product.serializers import ProductSerializer
from cheque.serializers import ChequeSerializer


def index(request):
    product = Product.objects.all()[:4]
    brand = Brand.objects.all()
    category = Category.objects.all()
    return render (request, 'main/index.html', {'product': product, 'brand': brand, 'category': category})


class ProductDetailView(DetailView):
    model = Product
    template_name = 'main/product_view.html'
    context_object_name = 'product'


class ChequeDetailView(DetailView):
    model = Cheque
    template_name = 'main/cheque_view.html'
    context_object_name = 'cheque'


class ChequeUpdateView(UpdateView):
    model = Cheque
    template_name = 'main/form.html'

    form_class = ChequeForm

class ChequeDeleteView(DeleteView):
    model = Cheque
    success_url = '/cheque'
    template_name = 'main/delete.html'

class ProductListView(ListView):
    paginate_by = 5
    model = Product
    template_name = 'main/product_list.html'
    context_object_name = 'product'


class ChequeListView(ListView):
    model = Cheque
    template_name = 'main/cheque_list.html'
    context_object_name = 'cheque'


def create(request):
    error = ''
    if request.method == 'POST':
        form = ChequeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('cheques')
        else:
            error = 'Форма не валидна'

    form = ChequeForm()

    data = {
        'form': form,
        'error': error
    }
    return render (request, 'main/form.html',data)

# class BrandAPIList(generics.ListCreateAPIView):
#     queryset = Brand.objects.all()
#     serializer_class = BrandSerializer

# class BrandAPIDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Brand.objects.all()
#     serializer_class = BrandSerializer

class BrandViewSet(viewsets.ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer

    @action(methods=['GET'],detail=False, url_path='filter')
    def get_filter(self,request):
        filter = self.request.query_params.get('filter')
        brand = Brand.objects.filter(country = filter)
        data = self.serializer_class(brand, many=True).data
        return Response(data)

class CategoryAPIListPagination(PageNumberPagination):
    page_size = 4
    page_size_query_param = 'page_size'
    max_page_size = 10

class CategoryAPIList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    pagination_class = CategoryAPIListPagination 

class CategoryAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class MarketViewSet(viewsets.ModelViewSet):
    queryset = Market.objects.all()
    serializer_class = MarketSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['city']

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    @action(methods=['POST'], detail=True, permission_classes=[IsAuthenticated], url_path='buy-now')
    def buy_now(self, request, pk):
        user_id = request.user
        product_id = self.queryset.filter(pk=pk)
        market_id = request.user.preferred_market
        description = 'Buy now cheque!'
        Cheque.objects.create(user_id=user_id, market_id=market_id, description = description).product_id.set(product_id)
        return Response({'message': 'add'})


class ChequeViewSet(viewsets.ModelViewSet):
    queryset = Cheque.objects.all()
    serializer_class = ChequeSerializer

    @action(methods=['GET'],detail=False, url_path='buy')
    def get_now(self, request):
        q = Q(description__startswith='Buy') | Q(description__startswith='Доставка')
        cheque = Cheque.objects.filter(q)
        data = self.serializer_class(cheque, many=True).data
        return Response(data)

    @action(methods=['GET'],detail=False, url_path='user', permission_classes=[IsAuthenticated])
    def get_by_user(self, request):
        user = request.user
        cheques = Cheque.objects.filter(user_id = user)
        data = self.serializer_class(cheques, many=True).data
        return Response(data)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=False, url_path='register')
    def register(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'message': 'sucsess'})

    @action(methods=['POST'], detail=False, url_path='login')
    def login(self, request):
        if 'email' not in request.data:
            raise ValidationError({'error': 'Please enter your email!'})
        if 'password' not in request.data:
            raise ValidationError({'error': 'Please enter your password!'})
        
        try:
            user = User.objects.get(email=request.data['email'])
        except User.DoesNotExist:
            raise NotFound({'error': 'The user with this email was not found. Please enter a valid email!'})

        if not user.check_password(request.data['password']):
            raise AuthenticationFailed({'error': 'This password is not correct. Check the login and enter the password again!'})

        refresh = RefreshToken.for_user(user)
        response = Response()
        response.data = {'acsess': str(refresh.access_token)}
        return response

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='me')
    def get_user(self,request):
        user = request.user
        data = self.serializer_class(user).data
        return Response(data)