from django.urls import path, include
from . import views

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'brand', views.BrandViewSet)
router.register(r'user', views.UserViewSet)
router.register(r'market', views.MarketViewSet)
router.register(r'product', views.ProductViewSet)
router.register(r'cheque', views.ChequeViewSet)

urlpatterns = [
    path('', views.index,name='home'),
    path('product/<int:pk>', views.ProductDetailView.as_view(), name='product-detail'),
    path('product', views.ProductListView.as_view(), name='products'),
    path('cheque/<int:pk>', views.ChequeDetailView.as_view(), name='cheque-detail'),
    path('cheque/<int:pk>/update', views.ChequeUpdateView.as_view(), name='cheque-update'),
    path('cheque/<int:pk>/delete', views.ChequeDeleteView.as_view(), name='cheque-delete'),
    path('cheque', views.ChequeListView.as_view(), name='cheques'),
    path('cheque/create', views.create, name='create'),

    path('api/v1/', include(router.urls)),
    path('api/v1/category/', views.CategoryAPIList.as_view()),
    path('api/v1/category/<int:pk>', views.CategoryAPIDetail.as_view())
]