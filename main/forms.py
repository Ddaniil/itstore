from cheque.models import Cheque
from django.forms import ModelForm, Select, Textarea, SelectMultiple


class ChequeForm(ModelForm):
    class Meta:
        model = Cheque
        fields = ['product_id','market_id','user_id','description']


        widgets = {
            "product_id": SelectMultiple(attrs={
                'class': 'form-control',
            }),
            "market_id": Select(attrs={
                'class': 'form-control',
            }),
            "user_id": Select(attrs={
                'class': 'form-control',
            }),
            "description": Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Описание',
                'rows': 7,
            }),
        }