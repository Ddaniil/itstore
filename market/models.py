from django.db import models
from simple_history.models import HistoricalRecords


class Market(models.Model):
    city = models.CharField(verbose_name='Город', max_length=255)
    street = models.CharField(verbose_name='Улица', max_length=255)
    house = models.CharField(verbose_name='Дом', max_length=255)
    underground = models.CharField(verbose_name='Метро', max_length=255)
    history = HistoricalRecords()


    def __str__(self):
        return f'{self.city} {self.street}'

    class Meta:
        verbose_name = 'Магазин'
        verbose_name_plural = 'Магазины'