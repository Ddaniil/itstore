from django.contrib import admin
from market.models import Market
from cheque.models import Cheque
from import_export.admin import ImportExportActionModelAdmin
from simple_history.admin import SimpleHistoryAdmin

class ChequeInline(admin.StackedInline):
    model = Cheque
    fields = ['created_at','product_id']
    raw_id_fields = ['product_id']


class MarketAdmin(ImportExportActionModelAdmin,SimpleHistoryAdmin):

    list_filter = ['city']
    list_display = ('city','underground','street')
    fieldsets = (
    ('Адрес магазина',{
        'fields':('city','street','house')
    }),
    (None,{
        'fields':['underground']
    }))

    inlines = [ChequeInline]


admin.site.register(Market, MarketAdmin)