from rest_framework import serializers

from .models import Market

class MarketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Market
        fields = "__all__"

class MarketCheque(serializers.ModelSerializer):
    class Meta:
        model = Market
        fields = ['city','house','street','underground']