from rest_framework import serializers

from .models import Brand

class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = "__all__"

class BrandProduct(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name','photo']

class BrandCheque(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name']