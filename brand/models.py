from django.db import models


class Brand(models.Model):
    name = models.CharField(verbose_name='Название бренда', max_length=255)
    country = models.CharField(verbose_name='Страна', max_length=255)
    photo = models.ImageField(verbose_name='Логотип', upload_to='brands')


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'