from django.contrib import admin
from brand.models import Brand
from product.models import Product

class ProductInline(admin.TabularInline):
    model = Product

class BrandAdmin(admin.ModelAdmin):
    list_display= ('name','country','id')
    list_filter = ['country']

    inlines = [ProductInline]

admin.site.register(Brand,BrandAdmin)