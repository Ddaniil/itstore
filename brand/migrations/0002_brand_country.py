# Generated by Django 4.0.5 on 2022-11-09 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brand', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='country',
            field=models.CharField(max_length=255, null=True, verbose_name='Страна'),
        ),
    ]
