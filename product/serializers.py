from rest_framework import serializers

from .models import Product

from category.serializers import CategoryProduct,CategoryCheque
from brand.serializers import BrandProduct,BrandCheque

class ProductSerializer(serializers.ModelSerializer):
    category_id = CategoryProduct(read_only=True)
    brand_id = BrandProduct(read_only=True)

    class Meta:
        model = Product
        fields = "__all__"

class ProductCheque(serializers.ModelSerializer):
    category_id = CategoryCheque(read_only=True)
    brand_id = BrandCheque(read_only=True)

    class Meta:
        model = Product
        fields = ['model','category_id','brand_id','price','rating']