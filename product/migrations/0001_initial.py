# Generated by Django 4.0.5 on 2022-06-27 18:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('category', '0001_initial'),
        ('brand', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=255, verbose_name='Модель')),
                ('description', models.TextField(verbose_name='Описание')),
                ('price', models.FloatField(verbose_name='Цена')),
                ('quanity', models.IntegerField(verbose_name='Количество')),
                ('rating', models.FloatField(verbose_name='Рейтинг')),
                ('photo', models.ImageField(upload_to='goods', verbose_name='Фото')),
                ('brand_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='brands', to='brand.brand', verbose_name='Бренд')),
                ('category_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='categories', to='category.category', verbose_name='Категория')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
    ]
