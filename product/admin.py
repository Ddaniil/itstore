from django.contrib import admin
from product.models import Product

class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
    ('О товаре',{
        'fields':('brand_id','category_id','model','description','photo')
    }),
    ('Характеристики товара',{
        'fields':('price','quanity','rating','option1','option2')
    })
    )
    list_display= ('brand_id','category_id','model','price','rating')
    list_filter = ('brand_id','category_id')
    raw_id_fields = ('brand_id','category_id')

admin.site.register(Product,ProductAdmin)