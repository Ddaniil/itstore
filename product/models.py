from django.db import models
from brand.models import Brand
from category.models import Category


class Product(models.Model):
    category_id = models.ForeignKey(Category, on_delete = models.CASCADE,verbose_name='Категория', related_name='categories')
    brand_id = models.ForeignKey(Brand, on_delete = models.CASCADE,verbose_name='Бренд', related_name='brands')
    model = models.CharField(verbose_name='Модель',max_length=255)
    description = models.TextField(verbose_name='Описание')
    price = models.FloatField(verbose_name='Цена')
    quanity = models.IntegerField(verbose_name='Количество')
    rating = models.FloatField(verbose_name='Рейтинг')
    photo = models.ImageField(verbose_name='Фото', upload_to='goods')
    option1 = models.CharField(verbose_name='Характеристика1',max_length=255)
    option2 = models.CharField(verbose_name='Характеристика2',max_length=255)

    def __str__(self):
        return f'{self.category_id.name} {self.model}'
    

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'