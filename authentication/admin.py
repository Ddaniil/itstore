from django.contrib import admin
from authentication.models import User

class UserAdmin(admin.ModelAdmin):
    fieldsets = (
        (None,{
            'fields':('first_name','last_name','email','preferred_market')
        }),
        ('Статус',{
            'fields':('is_active','is_staff','is_superuser')
        })
    )
    list_display= ('last_name','first_name','email')

admin.site.register(User,UserAdmin)
