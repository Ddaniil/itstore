from django.contrib import admin
from category.models import Category

class CategoryAdmin(admin.ModelAdmin):
    fieldsets = (
    (None,{
        'fields':('name','description')
    }),
    ('Характеристики категории товара',{
        'fields':[('option1','option2')]
    })
    )

admin.site.register(Category,CategoryAdmin)