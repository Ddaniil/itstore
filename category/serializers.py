from rest_framework import serializers

from .models import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"

class CategoryProduct(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name','option1','option2']

class CategoryCheque(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name']