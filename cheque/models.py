from django.db import models
from authentication.models import User
from market.models import Market
from product.models import Product
import datetime


class Cheque(models.Model):
    user_id = models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='Пользователь', related_name='users')
    market_id = models.ForeignKey(Market,on_delete=models.CASCADE,verbose_name='Магазин', related_name='markets')
    product_id = models.ManyToManyField(Product, verbose_name='Товар', related_name='products')
    created_at = models.DateField(verbose_name='Создан', default=datetime.date.today)
    description = models.TextField(verbose_name='Описание')


    def __str__(self):
        return f'Заказ №: {self.id}'
    
    def get_absolute_url(self):
        return f'/cheque/{self.id}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'