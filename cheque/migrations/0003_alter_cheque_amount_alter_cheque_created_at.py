# Generated by Django 4.0.5 on 2022-06-28 22:44

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cheque', '0002_remove_cheque_product_id_cheque_product_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cheque',
            name='amount',
            field=models.FloatField(default='0', verbose_name='Сумма'),
        ),
        migrations.AlterField(
            model_name='cheque',
            name='created_at',
            field=models.DateField(default=datetime.date.today, verbose_name='Создан'),
        ),
    ]
