# Generated by Django 4.0.5 on 2022-11-09 07:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cheque', '0004_remove_cheque_amount'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cheque',
            options={'verbose_name': 'Заказ', 'verbose_name_plural': 'Заказы'},
        ),
    ]
