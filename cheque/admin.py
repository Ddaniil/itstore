from django.contrib import admin
from cheque.models import Cheque
from market.models import Market

# class MarketInline(admin.TabularInline):
#     model = Market

class ChequeAdmin(admin.ModelAdmin):
    list_display = ('__str__','created_at','user_id','market_id')

    list_filter = ('created_at','market_id')

    fields = [('user_id','market_id'),'product_id','description','created_at']

    # inlines = [MarketInline]

admin.site.register(Cheque, ChequeAdmin)