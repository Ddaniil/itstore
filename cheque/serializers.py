from rest_framework import serializers

from .models import Cheque

from authentication.serializers import UserCheque
from product.serializers import ProductSerializer,ProductCheque
from market.serializers import MarketSerializer,MarketCheque

class ChequeSerializer(serializers.ModelSerializer):

    market_id = MarketCheque(read_only=True)
    product_id = ProductCheque(read_only=True, many=True)
    user_id = UserCheque(read_only=True)

    class Meta:
        model = Cheque
        fields = ['user_id','product_id','market_id','description']